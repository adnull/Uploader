<?php

/*------------------------------

Base for all responses

-------------------------------*/

class Response
{
	public function __construct()
	{
		header('content-type: application/json; charset=UTF-8');
	}
	
	public function check_api(&$apikey)
	{
		if (isset($apikey))
		{
			if ($user = get_user($apikey))
			{
				return $user;
			}
		}
	}

	public function error($description)
	{
		http_response_code(400);
		echo json_encode(array(
			'success' => false,
			'description' => $description
		), JSON_PRETTY_PRINT);
	}

	public function success($name, $body)
	{
		http_response_code(200);
		echo json_encode(array(
			'success' => true,
			$name => $body
		), JSON_PRETTY_PRINT);
	}
}

?>