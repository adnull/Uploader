<?php

/*------------------------------

Commonly used functions

-------------------------------*/

require_once 'config.php';

// Generate a random string with given length
function random_string($len) 
{
    $characters = UPLOAD_CHARSET;
    $characters_length = strlen($characters);
    $random_string = '';
    for ($i = 0; $i < $len; $i++) 
    {
        $random_string .= $characters[rand(0, $characters_length - 1)];
    }
    return $random_string;
}

// Whether or not an invite is valid
function is_invite_valid($invite)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM invites WHERE code = ? AND used IS NULL');
	$query->bind_param('s', $invite);
	$query->execute();
	$result = $query->get_result();
	$query->close();

	return $result->num_rows > 0;
}

// Whether or not a user exists
function user_exists($name)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM users WHERE name = ?');
	$query->bind_param('s', $name);
	$query->execute();
	$result = $query->get_result();
	$user = $result->fetch_assoc();
	$query->close();

	if ($result->num_rows == 1)
	{
		return $user;
	}
	return false;
}

// Check if a file with a given MD5 value exists
function md5_exists($md5)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM files WHERE md5 = ?');
	$query->bind_param('s', $md5);
	$query->execute();
	$result = $query->get_result();

	return $result->fetch_assoc();
}

// Get a user's information
function get_user($apikey)
{
	global $conn;

	// Retrieve info from the database.
	$query = $conn->prepare('SELECT * FROM users WHERE apikey = ?');
	$query->bind_param('s', $apikey);
	$query->execute();
	$result = $query->get_result();
	$user = $result->fetch_assoc();
	$query->close();

	// If the user is found, return their information.
	if ($result->num_rows == 1)
	{
		return $user;
	}
}

// Get a file's information from its new name
function get_file($name)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM files WHERE new_name = ?');
	$query->bind_param('s', $name);
	$query->execute();
	$result = $query->get_result();

	return $result->fetch_assoc();
}

// Get a user's uploaded files as a list
function get_files($user, $page)
{
	global $conn;

	$start = floor($page) * FILE_FETCH_LIMIT;

	$query = $conn->prepare('SELECT * FROM `files` WHERE user = ? LIMIT 50 OFFSET ?');
	$query->bind_param('ii', $user['id'], $start);
	$query->execute();
	$result = $query->get_result();

	$files = [];

	while ($file = $result->fetch_assoc())
	{
		$files[] = $file;
	}

	return $files;
}

// Get a specific invite code
function get_invite($code)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM invites WHERE code = ?');
	$query->bind_param('s', $code);
	$query->execute();
	$result = $query->get_result();

	return $result->fetch_assoc();
}

// Get all of a user's invite codes
function get_invites($user)
{
	global $conn;

	$query = $conn->prepare('SELECT * FROM invites WHERE inviter = ?');
	$query->bind_param('i', $user['id']);
	$query->execute();
	$result = $query->get_result();

	$invites = [];

	while ($invite = $result->fetch_assoc())
	{
		$invites[] = array(
			'id' => $invite['id'], 
			'code' => $invite['code'], 
			'invitee' => $invite['invitee'],
			'created' => $invite['created'],
			'used' => $invite['used']
		);
	}

	return $invites;
}

// Create a user with a given username and password
function create_user($name, $pass)
{
	global $conn;

	// Encrypt password and make an API key.
	$apikey = random_string(API_LENGTH);
	$encrypted_pass = hash('sha512', $pass);

	// Put the user in the database.
	$query = $conn->prepare('INSERT INTO users (name, pass, apikey) VALUES (?, ?, ?)');
	$query->bind_param('sss', $name, $encrypted_pass, $apikey);
	$query->execute();
	$query->close();

	return get_user($apikey);
}

// Create an invite for a user
function create_invite($user)
{
	global $conn;

	// Generate a random invite code
	$code = random_string(INVITE_LENGTH);

	$query = $conn->prepare('INSERT INTO invites (code, inviter) VALUES (?, ?)');
	$query->bind_param('si', $code, $user['id']);
	$query->execute();
	$query->close();

	return get_invite($code);
}

// Put a file's information into the database
function create_file($user, $old_name, $new_name)
{
	global $conn;

	$md5 = md5_file(FILE_LOCATION . $new_name);
	
	// Check if the file name is greater than 32 characters
	if (strlen($old_name) > 32)
	{
		$old_name = mb_substr($old_name, 0, 32);
	}
	
	$query = $conn->prepare('INSERT INTO files (user, original_name, new_name, md5) VALUES (?, ?, ?, ?)');
	$query->bind_param('isss', $user['id'], $old_name, $new_name, $md5);
	$query->execute();
	$query->close();

	return get_file($new_name);
}

// Delete a file from the database given its new name
function delete_file($file)
{
	global $conn;

	$query = $conn->prepare('DELETE FROM files WHERE new_name = ?');
	$query->bind_param('s', $file);
	$query->execute();
	$query->close();
}

// Re-order the files into a better format
function redo_files(&$file_post) {
	$file_array = array();
	$file_count = count($file_post['name']);
	$file_keys = array_keys($file_post);

	for ($i = 0; $i < $file_count; $i++) {
		foreach ($file_keys as $key) {
			$file_array[$i][$key] = $file_post[$key][$i];
		}
	}

	return $file_array;
}