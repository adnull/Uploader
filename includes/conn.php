<?php

/*------------------------------

Start the connection to the SQL server

-------------------------------*/

require_once 'config.php';

// Start up a new MySql connection
$conn = new mysqli(DB_CONN, DB_USER, DB_PASS, DB_TABLE);

// Handle any errors when connecting
if ($conn->connect_error)
{
	die('Connection to the database failed :c');
}

?>