<?php

/*------------------------------

Adjust certain settings for the API

-------------------------------*/

// Database configuration
define('DB_CONN', 'address_of_db');
define('DB_USER', 'db_username');
define('DB_PASS', 'db_password');
define('DB_TABLE', 'db_table');

// Length of the API key/the randomized filenames for uploads
define('API_LENGTH', 32);
define('FILE_LENGTH', 5);
define('INVITE_LENGTH', 32);

// Max amount of files to show when listing the user's file
define('FILE_FETCH_LIMIT', 50);

// Upload URL must contain a trailing slash
define('UPLOAD_URL', 'https://example.com/files/');

// Where the files will save to, including trailing slash
define('FILE_LOCATION', '/var/www/files/');

// All of the possible characters for the randomized filenames
define('UPLOAD_CHARSET', 'abcdefghijklmnopqrstuvwxyz1234567890');

// Maximum amount of times to look for a filename before giving up
define('MAX_RETRIES', 10);

// Blacklisted extensions
$blacklist = array('exe', 'dll', 'sh', 'bat', 'msi', 'jar');

// Double dotted extensions
$double_dotted = array('tar.gz');

?>