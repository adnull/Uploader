<?php

/*------------------------------

Set up the admin/first account for the DB

-------------------------------*/

require_once 'includes/config.php';
require_once 'includes/conn.php';
require_once 'includes/functions.php';
require_once 'classes/response.php';

$response = new Response();

if (isset($_POST['name'], $_POST['pass']))
{
	// Create the user
	$created_user = create_user($_POST['name'], $_POST['pass']);
	$response->success('user', array(
		'uid' => $created_user['id'],
		'name' => $created_user['name'],
		'apikey' => $created_user['apikey'],
		'date_joined' => $created_user['join_date']
	));
	exit();
}
else
{
	header('content-type: text/html; charset=UTF-8');
}

?>
<!DOCTYPE html>
<html>
<body>
<form action="install.php" method="POST">
	<h2>Admin User</h2>
	Username: <input type="text" name="name"><br/>
	Password: <input type="password" name="pass"><br/>
	<input type="submit" value="Submit">
	<p><strong>Remember to delete this file after setup!</strong></p>
</form>
</body>
</html>
