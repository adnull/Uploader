<?php

/*------------------------------

Upload all of the given files to the server

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/config.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
	http_response_code(400);
	die('Invalid request method.');
}

function upload($file, $user)
{
	global $double_dotted;
	global $blacklist;

	// Get the information on the file
	$info = pathinfo($file['name']);
	
	// Cut off the extension if it's too long
	if ((FILE_LENGTH + 1 + strlen($info['extension'])) > 32)
	{
		$info['extension'] = substr($info['extension'], 0, (31 - FILE_LENGTH));
	}

	// Check for special extensions with two dots or more
	foreach ($double_dotted as $extension)
	{
		if (strpos($file['name'], $extension) !== false)
		{
			$info['extension'] = $extension;
			break;
		}
	}

	// Check if the file uploaded has a blacklisted extension
	if (in_array($info['extension'], $blacklist))
	{
		throw new Exception('Files with the specified extension are blacklisted');
	}

	// Check if file has already been uploaded
	if ($md5_file = md5_exists(md5_file($file['tmp_name'])))
	{
		return $md5_file;
	}

	$max_retries = MAX_RETRIES;

	// Generate a random file name
	do {
		if ($max_retries-- == 0)
		{
			throw new Exception("Couldn't find a possible filename");
		}

		$new_file_name = random_string(FILE_LENGTH) . '.' . $info['extension'];
		$result = get_file($new_file_name);
	} while ($result);

	// Construct the new file location
	$target = FILE_LOCATION . $new_file_name;

	// Try to move the file to the target location
	if (move_uploaded_file($file['tmp_name'], $target))
	{
		return create_file($user, $file['name'], $new_file_name);
	}
	else
	{
		throw new Exception('File could not be uploaded');
	}
}

$response = new Response();

if ($user = $response->check_api($_POST['apikey']))
{
	if (isset($_FILES['files']))
	{
		$file_array = redo_files($_FILES['files']);
		$uploaded_files = [];

		try
		{
			foreach ($file_array as $file)
			{
				$uploaded_files[] = upload($file, $user);
			}

			$response->success('files', $uploaded_files);
		}
		catch (Exception $e)
		{
			$response->error($e->getMessage());
		}
	}
	else
	{
		$response->error('No files were provided');
	}
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>