<?php

/*------------------------------

Deletes a file from the server

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/config.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

function delete($file)
{
	if (get_file($file))
	{
		delete_file($file);
		unlink(FILE_LOCATION . $file);
	}
	else
	{
		throw new Exception('File does not exist');
	}
}

$response = new Response();

if ($user = $response->check_api($_GET['apikey']))
{
	try
	{
		delete($_GET['file']);
		$response->success('message', 'File was successfully deleted');
	}
	catch (Exception $e)
	{
		$response->error($e->getMessage());
	}
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>