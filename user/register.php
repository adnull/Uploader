<?php

/*------------------------------

Allow a user to register with the info they want

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'POST')
{
	http_response_code(400);
	die('Invalid request method.');
}

function register($name, $pass, $invite)
{
	global $conn;
	
	// Throw exception if username is not alpha-numeric.
	if (!ctype_alnum($name))
	{
		throw new Exception('Usernames may only contain letters and numbers');
	}

	// Throw exception if length of username is longer than 32.
	if (strlen($name) > 32)
	{
		throw new Exception('Username is too long (maximum length is 32)');
	}

	// Throw exception if invite is invalid.
	if (!is_invite_valid($invite))
	{
		throw new Exception('Invite is invalid');
	}

	// Throw exception if username already exists.
	if (user_exists($name))
	{
		throw new Exception('Username already exists');
	}

	// Create the user then get their info.
	$created_user = create_user($name, $pass);

	// Use the invite
	$query = $conn->prepare('UPDATE invites SET `invitee` = ?, `used` = now() WHERE `code` = ?');
	$query->bind_param('is', $created_user['id'], $invite);
	$query->execute();
	$query->close();

	// Return the user's info to them.
	return array(
		'uid' => $created_user['id'],
		'name' => $created_user['name'],
		'apikey' => $created_user['apikey'],
		'date_joined' => $created_user['join_date']
	);
}

$response = new Response();

if (isset($_POST['name'], $_POST['pass'], $_POST['invite']))
{
	try
	{
		$user = register($_POST['name'], $_POST['pass'], $_POST['invite']);
		$response->success('user', $user);
	}
	catch (Exception $e)
	{
		$response->error($e->getMessage());
	}
}
else
{
	$response->error('Missing required data (name, pass, invite)');
}

$conn->close();

?>