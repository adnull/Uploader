<?php

/*------------------------------

Returns all info on a specific user

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

$response = new Response();

if ($user = $response->check_api($_GET['apikey']))
{
	$response->success('user', array(
		'uid' => $user['id'],
		'name' => $user['name'],
		'apikey' => $user['apikey'],
		'date_joined' => $user['join_date']
	);
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>