<?php

/*------------------------------

Gives a user's API key from their username and password

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

function login($name, $pass)
{
	global $conn;

	// Encrypt password
	$encrypted_pass = hash('sha512', $pass);

	// Validate the user's password
	$query = $conn->prepare("SELECT apikey, pass FROM users WHERE name = ? AND pass = ?");
	$query->bind_param("ss", $name, $encrypted_pass);
	$query->execute();
	$query->store_result();
	$query->bind_result($apikey, $password);
	$query->fetch();
	$query->close();

	if ($password == $encrypted_pass)
	{
		return $apikey;
	}
}

$response = new Response();

if (isset($_GET['name'], $_GET['pass']))
{
	if ($apikey = login($_GET['name'], $_GET['pass']))
	{
		$response->success('apikey', $apikey);
	}
	else
	{
		$response->error('Invalid username or password');
	}
}
else
{
	$response->error('Missing required data (name, pass)');
}

$conn->close();

?>