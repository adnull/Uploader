<?php

/*------------------------------

Gives an array of all of the user's uploaded files

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

$response = new Response();

if ($user = $response->check_api($_GET['apikey']))
{
	$page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 0;
	$response->success('files', get_files($user, $page));
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>