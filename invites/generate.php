<?php

/*------------------------------

Generates an invite to the service

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

$response = new Response();

if ($user = $response->check_api($_GET['apikey']))
{
	$invites = get_invites($user);

	$join = strtotime($user['join_date']);
	$now = strtotime(date('Y-m-d H:i:s'));

	$year1 = date('Y', $join);
	$year2 = date('Y', $now);

	$month1 = date('m', $join);
	$month2 = date('m', $now);

	$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
	$invites_left = $diff - sizeof($invites);

	if ($invites_left > 0)
	{
		$invite = create_invite($user);
		$response->success('invite', $invite);
	}
	else
	{
		$response->error("You don't have any more invite codes left");
	}
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>