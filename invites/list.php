<?php

/*------------------------------

Lists all of the user's used invites

-------------------------------*/

require_once '../includes/conn.php';
require_once '../includes/functions.php';
require_once '../classes/response.php';

if ($_SERVER['REQUEST_METHOD'] != 'GET')
{
	http_response_code(400);
	die('Invalid request method.');
}

$response = new Response();

if ($user = $response->check_api($_GET['apikey']))
{
	$invites = get_invites($user);
	$response->success('invites', $invites);
}
else
{
	$response->error('Invalid API key');
}

$conn->close();

?>