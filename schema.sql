CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `original_name` varchar(32) NOT NULL,
  `new_name` varchar(32) NOT NULL,
  `md5` varchar(32) NOT NULL,
  `date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;

CREATE TABLE `invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `inviter` int(11) NOT NULL,
  `invitee` int(11) DEFAULT NULL,
  `created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `pass` varchar(512) NOT NULL,
  `apikey` varchar(32) NOT NULL,
  `join_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;
